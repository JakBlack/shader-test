﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/MorphDome"
{
    Properties
    {
        _Color ("Color", Color) = (1, 1, 1, 1)
        _NoiseTex ("Noise Texture", 2D) = "white" {}
        _Extrusion ("Extrusion Amount", Range(-1, 1)) = 0.0
        _NoiseCutoff ("Noise cutoff", Range(0, 1)) = 0.0
        _ScrollSpeed ("Scroll speed", Vector) = (0, 0, 0, 0)
    }
    SubShader
    {
        Tags { "RenderType"="TransparentCutout" "Queue"="Transparent" }
        Cull Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma geometry geom

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
            };

            float _Extrusion;
            float _NoiseCutoff;
            float4 _ScrollSpeed;
            fixed4 _Color;
            sampler2D _NoiseTex;

            v2f vert (appdata v)
            {
                v2f o;
                o.color = float4(v.normal, 1) * 0.5 + 0.5;
                o.vertex = v.vertex;
                return o;
            }
            
            [maxvertexcount(3)]
            void geom(triangle v2f input[3], inout TriangleStream<v2f> OutputStream)
            {
                v2f output = (v2f)0;
                float4 vertexMiddle = (input[0].vertex + input[1].vertex + input[2].vertex) / 3;

                float4 noise = tex2Dlod(_NoiseTex, vertexMiddle * _Time * _ScrollSpeed).x;
                noise = exp(noise);
                // SinTime is for "breathing effect", better to test without it
                float extrusion = _Extrusion * noise;// * (_SinTime.a * 0.5 + 0.5);
               
                for(int i = 0; i < 3; i++)
                {
                    float4 vert = input[i].vertex;
                    float4 vertNormalized = normalize(vert);
                    
                    vert.xyz += vertNormalized * extrusion;
                    
                    output.vertex = UnityObjectToClipPos(vert);
                    output.color = input[i].color;
                    OutputStream.Append(output);
                }
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return i.color * _Color;
            }
            ENDCG
        }
    }
}
